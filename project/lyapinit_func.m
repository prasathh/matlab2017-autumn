%This function generates (N+1)^2 points on a sphere of radius R 
%centered at (x0,y0,z0)
function [x,y,z]=lyapinit_func(x0,y0,z0,N,R);
    
    [x,y,z]=sphere(N); %sphere of unit radius centered at origin
    
    %scale radius and shift sphere to (x0,y0,z0)
    x=x*R+x0;
    y=y*R+y0;
    z=z*R+z0;
    
    %return coordinates in column vector with size (N+1)^2
    N2=(N+1)^2;
    x=reshape(x,N2,1);
    y=reshape(y,N2,1);
    z=reshape(z,N2,1);
    
return;