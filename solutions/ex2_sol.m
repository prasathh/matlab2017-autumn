%Solution to exercise 2
%Consider the simple linear predator-prey model:
%dr/dt = -(a/b)f
%df/dt = (b/a)r
%
%Here, r and f represent departures from the equilibrium rabbit and fox
%populations. In the following, set the parameters a=0.1, b=0.2

%1. Calculate the eigenvalues and eigenvectors of the propagator matrix, A
a = 0.1;
b=0.2;
A = [0 -a/b;b/a 0];
[V,D]=eig(A);

x1 = V(:,1); %1st eigenvector
x2 = V(:,2); %2nd eigenvector
l1 = D(1,1); %1st eigenvalue
l2 = D(2,2); %2nd eigenvalue

%2. Use the det function to verify that the eigenvalues are correct.
I=eye(2);
check_l1 = det(A-l1*I)
check_l2 = det(A-l2*I)

%3. Verify that the eigenvectors are correct by computing Ax=lx
check_x1 = A*x1 - l1*x1
check_x2 = A*x2 - l2*x2

%4. Take the initial conditions to be, r0=0.2, f0=0.1, and find the
%integration constants of the system.

%Soln: [r;f] = c1*x1*exp(l1*t) + c2*x2*exp(l2*t)
%so, [r0;f0] = c1*x1 + c2*x2
%or [r0;f0] = [x1 x2][c1;c2]
%and since V = [x1 x2]:

r0=0.2;
f0=0.1;
C = V\[r0;f0]
c1 = C(1);
c2 = C(2);

%5. Finally, compute r(t) and f(t) for 0<=t<=10, and plot the results.
t = linspace(0,10,1000);
x = c1*x1*exp(l1*t) + c2*x2*exp(l2*t);
r = x(1,:); 
f = x(2,:);

figure
plot(t,r,'-')
hold on
plot(t,f,'r--')
set(gca,'fontsize',16)
legend('rabbits','foxes')
xlabel('time')
ylabel('population')
title('Ex. 2, prob. 5')
