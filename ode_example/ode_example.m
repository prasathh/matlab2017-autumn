%integrate ODE dy/dt = -ay

%set a
a=0.1;

%set time interval for integration
tspan=[0 2];

%set initial condition
y0=1;

%set ODE45 options
OPTIONS = odeset('reltol',1.0e-8,'abstol',1.0e-8); %reduce error tolerances, use help odeset for more info

%call ODE45
[t,y] = ode45(@(t,y) ode_func(t,y,a),tspan,y0,OPTIONS);

%construct exact solution for comparison
yexact = y0*exp(-a*t);

%compute error
error = max(abs(yexact-y))