# README #

Files for matlab workshop for CDT students, Autumn, 2017.
### Included in this repository ###

* Slides 
* Exercises
* Solutions to exercises
* Example files for solving a simple initial value problem (in directory, ode_example)
* Project description and example files for creating an animation (in directory, project)
